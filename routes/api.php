<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Http\Controllers\tetsController;

Route::get('/user', function (Request $request) {
    return $request->user();
});

//Route::get('/wallet', [tetsController::class, 'show']);

Route::get('wallet', [
    'as' => 'users.wallet',
    'uses' => 'tetsController@show']);


//Route::group([
//], function() {
//    Route::get('/samwallet', [
//        'uses'	=> 'tetsController@sam'
//    ]);
//});