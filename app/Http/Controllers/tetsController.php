<?php

namespace App\Http\Controllers;

use App\orders;
use App\wallet;
use Illuminate\Http\Request;
use Validator;

class tetsController extends Controller
{
    //
    public function show(Request $request)
    {
        $customMessages = [
            'required' => 'The :attribute field is required.'
        ];

        $this->validate($request, [
            'walletid'      => 'required|numeric',
            'amount'     => 'required|digits_between:1,10',
            'fee'     => 'required',
            'symbol'  => 'required|string|min:3|max:3',
        ], $customMessages);

        $wallet = wallet::find($request->walletid);
        if(!empty($wallet)){
            $wallet_balance = $wallet->balance;
            $walletcoin = strtoupper($wallet->symbol);
            $reqcoin = strtoupper($request->symbol);
            if($walletcoin == $reqcoin){
                $isvalidate = true;
            }else{
                return ["errror" => 3, "msg" =>"symbol err"];
            }


            if($wallet_balance < $request->amount){
                return ["errror" => 2, "msg" =>"balance err"];
            }else{
                $order = new orders();
                $order->user_id = 1;
                $order->currency_id = 1;
                $order->amount = $request->amount;
                $order->fee = $request->fee;
                $order->save();
                $wallet->balance = $wallet_balance - $request->amount;
                $wallet->save();
            }

        }else{
            return ["errror" => 2, "msg" =>"wallet not found"];
        }
        return $wallet_balance;
    }

    public function sam(Request $request)
    {
        return $request->all();
    }
}
