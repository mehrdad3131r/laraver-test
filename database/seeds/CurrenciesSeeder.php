<?php

use App\currency;
use Illuminate\Database\Seeder;

class CurrenciesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        currency::create([
            "id" => 1,
            "name" => "Bitcoin",
            "symbol" => "btc",
            "fee" => 100
        ]);
        currency::create([
            "id" => 2,
            "name" => "Ethereum",
            "symbol" => "eth",
            "fee" => 100,
        ]);
        currency::create([
            "id" => 3,
            "name" => "Tron",
            "symbol" => "trx",
            "fee" => 100,
        ]);
        \App\wallet::create([
            "id" => 1,
            "user_id" => 1,
            "currency_id" => 1,
            "address" => "dsfgdghsdfgsdfuh",
            "balance" => 100,
            "symbol" => "btc"
        ]);

        //
    }
}
